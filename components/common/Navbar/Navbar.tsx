import { FC } from 'react'
import Link from 'next/link'
import { Logo, Container } from '@components/ui'
import { Searchbar, UserNav } from '@components/common'
import s from './Navbar.module.css'

const Navbar: FC = () => (
  <Container className="bg-cutout bg-cover bg-no-repeat bg-opacity-40 h-screen">
    <div className="relative flex flex-row justify-between py-4 align-center md:py-6">
      <div className="flex items-center flex-1">
        <Link href="/">
          <a className={s.logo} aria-label="Logo">
            <Logo />
          </a>
        </Link>
      </div>

      <div className="flex justify-end flex-1 mr-10 space-x-8">
        <nav className="hidden ml-6 space-x-4 lg:block">
          <Link href="/search">
            <a className={s.link}>All</a>
          </Link>
          <Link href="/search?q=clothes">
            <a className={s.link}>Clothes</a>
          </Link>
          <Link href="/search?q=accessories">
            <a className={s.link}>Accessories</a>
          </Link>
          <Link href="/search?q=shoes">
            <a className={s.link}>Shoes</a>
          </Link>
        </nav>
      </div>
      <UserNav />
    </div>

    <div className="container mx-auto max-w-screen-xl my-20">
      <div className="flex flex-wrap overflow-hidden md:-mx-4">
        <div className="w-full overflow-hidden md:my-4 md:px-4 md:w-1/2">
          <h2 className="text-charcoal font-semibold text-3xl tracking-widest mb-5">
            SOME DAYS, LIFE CAN BE{' '}
            <span className="text-secondary text-3xl">RUFF</span>.
          </h2>
          <p className="dinosaur text-charcoal font-semibold mb-5">
            They give you love, so lets give them healthy, wholesome treats that
            are hand made with ingredients locally sourced in Alaska. We use
            beer barley from a local brewery combined with our key ingredients
            giving energy-boosting nutrients and a taste your dog will enjoy.
          </p>

          <p className="dinosaur text-charcoal font-semibold  mb-5">
            A great choice for that active outdoors dog-owner duo or a loving
            family pet that fills your day with smiles.
          </p>

          <p className="dinosaur text-charcoal font-semibold  mb-5">
            We are their whole world, so let's give them the best it has to
            offer.
          </p>

          <button className="self-center object-center mx-auto py-3 px-7 mt-10 border-2 border-secondary bg-secondary font-bold text-white hover:border-white transition duration-500 ease-in-out">
            <a
              href="https://tailwindcss.com/docs"
              className="text-cyan-600 hover:text-cyan-700"
            >
              {' '}
              GET STARTED{' '}
            </a>
          </button>
        </div>

        <div className="w-full overflow-hidden md:my-4 md:px-4 md:w-1/2">
          <img className="h-2/5" src="/1.png" />
        </div>
      </div>
    </div>

    {/* 
      <div className="flex pb-4 lg:px-6 lg:hidden">
        <Searchbar id="mobile-search" />
      </div> */}
  </Container>
)

export default Navbar
