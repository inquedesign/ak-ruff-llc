import cn from 'classnames'
import dynamic from 'next/dynamic'
import s from './Layout.module.css'
import { useRouter } from 'next/router'
import React, { FC } from 'react'
import { useUI } from '@components/ui/context'
import { Navbar, NavbarInternal, Footer } from '@components/common'
import { useAcceptCookies } from '@lib/hooks/useAcceptCookies'
import { Sidebar, Button, Modal, LoadingDots } from '@components/ui'
import CartSidebarView from '@components/cart/CartSidebarView'

import LoginView from '@components/auth/LoginView'
import { CommerceProvider } from '@framework'
import type { Page } from '@framework/common/get-all-pages'

const Loading = () => (
  <div className="w-80 h-80 flex items-center text-center justify-center p-3">
    <LoadingDots />
  </div>
)

const dynamicProps = {
  loading: () => <Loading />,
}

const SignUpView = dynamic(
  () => import('@components/auth/SignUpView'),
  dynamicProps
)

const ForgotPassword = dynamic(
  () => import('@components/auth/ForgotPassword'),
  dynamicProps
)

const FeatureBar = dynamic(
  () => import('@components/common/FeatureBar'),
  dynamicProps
)

interface Props {
  pageProps: {
    pages?: Page[]
    commerceFeatures: Record<string, boolean>
  }
}

const Layout: FC<Props> = ({
  children,
  pageProps: { commerceFeatures, ...pageProps },
}) => {
  const {
    displaySidebar,
    displayModal,
    closeSidebar,
    closeModal,
    modalView,
  } = useUI()
  const { acceptedCookies, onAcceptCookies } = useAcceptCookies()
  const { locale = 'en-US' } = useRouter()
  const router = useRouter()
  const showHeader = router.pathname === '/' ? true : false
  const showHeaderInternal = router.pathname === '/' ? false : true
  return (
    <CommerceProvider locale={locale}>
      <div className={cn(s.root)}>
        {showHeader && <Navbar />}
        {showHeaderInternal && <NavbarInternal />}
        <main className="fit">{children}</main>
        <Footer pages={pageProps.pages} />

        <Modal open={displayModal} onClose={closeModal}>
          {modalView === 'LOGIN_VIEW' && <LoginView />}
          {modalView === 'SIGNUP_VIEW' && <SignUpView />}
          {modalView === 'FORGOT_VIEW' && <ForgotPassword />}
        </Modal>

        <Sidebar open={displaySidebar} onClose={closeSidebar}>
          <CartSidebarView />
        </Sidebar>

        <FeatureBar
          title="This site uses cookies to improve your experience. By clicking, you agree to our Privacy Policy."
          hide={acceptedCookies}
          action={
            <Button className="mx-5" onClick={() => onAcceptCookies()}>
              Accept cookies
            </Button>
          }
        />
      </div>
      <script
        dangerouslySetInnerHTML={{
          __html: `
        <div id='rebillia_overlay' style="z-index:-1; display:none;"></div><script type="text/javascript">var currentcustomeremail = '%%GLOBAL_CurrentCustomerEmail%%';var shopPath = '%%GLOBAL_ShopPathSSL%%';var domainName = 'https://app.rebillia.com';var domainURL = 'https://app.rebillia.com/';var appClientId = "ageychj33ynffm8otcbd88hxt9spicx";var rebpostxmlhttpURL = 'https://app.rebillia.com/login/customers';var rebxmlhttpUrl = 'https://app.rebillia.com/login/customers/';var date = new Date();date.setTime(date.getTime()+(30*60*1000));var expires = "; expires="+date.toGMTString();$(document).ready(function(){var matches=window.location.href.match(/[^\/]+$/);if (matches && matches.length) {var vec=(matches[0].split('.'));if (vec[0] == 'account' || vec[0] == 'checkout'  || vec[0] == 'finishorder'){$.getScript(domainName+"/js/embed-"+ vec[0] +".js");}if (vec[0] == 'account'){customerJWT();}}});function customerJWT(bc_email) {var xmlhttp = new XMLHttpRequest();xmlhttp.onreadystatechange = function() {if (xmlhttp.readyState == 4 ) {if (xmlhttp.status == 200) {var rebxmlhttp = new XMLHttpRequest();rebxmlhttp.onreadystatechange = function() {if (rebxmlhttp.readyState == 4 ) {if (rebxmlhttp.status == 200) {console.log('Rebillia Message. Authentication Success');var data = JSON.parse(rebxmlhttp.responseText);document.cookie ="rebillia_token="+data.token+expires+"; path=/";}else if (rebxmlhttp.status == 404) {console.log('Rebillia Message. Token Error');}else {console.log('Rebillia Message. Authentication Failed');}}};rebxmlhttp.open("GET", rebxmlhttpUrl+xmlhttp.responseText, true);rebxmlhttp.withCredentials = true;rebxmlhttp.send();} else if (xmlhttp.status == 404) {var rebpostxmlhttp = new XMLHttpRequest();rebpostxmlhttp.onreadystatechange = function() {if (rebpostxmlhttp.readyState == 4 ) {if (rebpostxmlhttp.status == 200) {console.log('Rebillia Message. Authentication Success. Customer login successfully.');var data = JSON.parse(rebpostxmlhttp.responseText);document.cookie ="rebillia_token="+data.token+expires+"; path=/";}}};params="bc_email="+bc_email;rebpostxmlhttp.open("POST", rebpostxmlhttpURL, true);rebpostxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");rebpostxmlhttp.withCredentials = true;rebpostxmlhttp.send(params);} else {console.log('Rebillia Message. Something went wrong');}}};xmlhttp.open("GET", "/customer/current.jwt?app_client_id="+appClientId, true);xmlhttp.send();}</script>
      `,
        }}
      />
    </CommerceProvider>
  )
}

export default Layout
