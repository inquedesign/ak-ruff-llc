import s from './Midroll.module.css'

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { Navigation } from 'swiper/core'

// Install modules
SwiperCore.use([Navigation])

const FlavorSlider = ({ className = '', ...props }) => (
  <div className="dinosaur h-3/6 w-full bg-green">
    <Swiper
      spaceBetween={50}
      slidesPerView={1}
      onSlideChange={() => console.log('slide change')}
      onSwiper={(swiper) => console.log(swiper)}
      navigation
      className="w-3/4 h-5/6"
    >
      <SwiperSlide className="text-white p-20 bg-green">
        <div className="flex flex-wrap overflow-hidden md:-mx-1 lg:-mx-4 xl:-mx-4">
          <div className="w-2/6 overflow-hidden md:my-1 md:px-1 md:w-1/2 lg:my-4 lg:px-4 lg:w-1/2 xl:my-4 xl:px-4 xl:w-2/6">
            <div className="w-5/6">
              <img src="/peanutapple.png" />
            </div>
          </div>

          <div className="w-full overflow-hidden md:my-1 md:px-1 md:w-1/2 lg:my-4 lg:px-4 lg:w-w-2/6 xl:my-4 xl:px-4 xl:w-1/2">
            <div>
              <h2 className="dinosaur font-bold text-charcoal text-2xl mb-5">
                APPLE <span className="text-secondary text-3xl">+</span> PEANUT
                BUTTER
              </h2>
            </div>
            <div>
              <p className="dinosaur text-charcoal font-semibold">
                Apple + Peanut Butter treats are packed with vitamins and
                nutrients for your dog with a wonderful sweet, nutty smell. The
                beer barleys' process allows for easier digestion and an
                increase in trace nutrients. While the pure coconut MCT oil
                combined with the apple and peanut butter provide vitamins and
                support to your dog's skin, fur, brain, and joint health.
              </p>
            </div>
            <button className="py-3 px-7 mt-12 w-max border-2 border-secondary bg-secondary font-bold text-white hover:border-white transition duration-500 ease-in-out">
              <a
                href="https://tailwindcss.com/docs"
                className="text-cyan-600 hover:text-cyan-700"
              >
                {' '}
                EXPLORE THE FLAVORS{' '}
              </a>
            </button>
          </div>
        </div>
      </SwiperSlide>
      <SwiperSlide>Slide 2</SwiperSlide>
      <SwiperSlide>Slide 3</SwiperSlide>
    </Swiper>
  </div>
)

export default FlavorSlider
