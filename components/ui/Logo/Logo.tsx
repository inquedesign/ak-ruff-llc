import s from './Logo.module.css'

const Logo = ({ className = '', ...props }) => (
  <img className={s.logo} src="/ruff.svg" alt="Alaska Ruff" />
)

export default Logo
