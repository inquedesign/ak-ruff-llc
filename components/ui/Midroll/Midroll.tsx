import s from './Midroll.module.css'

const Midroll = ({ className = '', ...props }) => (
  <div className="relative bg-mid-roll bg-center py-40 px-20">
    <div className="max-w-md ">
      <div className="py-8 text-base leading-6 space-y-10 text-gray-700 sm:text-lg sm:leading-7">
        <p className="font-bold">
          We’re just <span className="text-secondary">two good friends</span>{' '}
          that put their heads together to create an amazingly healthy dog treat
          — with a focus on <span className="text-secondary">nutrition</span>,{' '}
          <span className="text-secondary">limited ingredients</span>,{' '}
          <span className="text-secondary">and your dog’s happiness</span>.
        </p>

        <button className="py-3 px-7 w-max border-2 border-secondary bg-secondary text-white hover:border-white transition duration-500 ease-in-out">
          <a
            href="https://tailwindcss.com/docs"
            className="text-cyan-600 hover:text-cyan-700"
          >
            {' '}
            Our Story{' '}
          </a>
        </button>
      </div>
    </div>
  </div>
)

export default Midroll
