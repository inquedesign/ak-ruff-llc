import s from './TestimonialBlock.module.css'

const TestimonialBlock = ({ className = '', ...props }) => (
  <div className=" w-8/12 md:w-6/12 mx-auto mt-20 mb-20">
    <div className="flex justify-center items-center content-center flex-wrap  -mx-2 overflow-hidden md:-mx-2 lg:-mx-2 xl:-mx-2">
      <div className="my-2 px-2 w-full overflow-hidden md:my-2 md:px-2 md:w-full lg:my-2 lg:px-2 lg:w-full xl:my-2 xl:px-2 xl:w-full">
        <h2 className="text-center text-charcoal font-semibold text-2xl mb-3">
          EVERYONE’S BARKING ABOUT{' '}
          <span className="text-secondary">ALASKA RUFF</span>
        </h2>
      </div>

      <div className="my-2 px-2 w-full overflow-hidden md:my-2 md:px-2 md:w-full lg:my-2 lg:px-2 lg:w-full xl:my-2 xl:px-2 xl:w-full">
        <p className="text-center text-sm text-charcoal font-semibold ">
          <span className="text-secondary">“</span>BEST DOG TREATS OUT THERE!
          OUR PUPS ABSOLUTELY LOVE THEM... EACH AND EVERY FLAVOR!
          <span className="text-secondary">“</span>
        </p>
      </div>

      <div className="my-2 px-2 w-8/12 overflow-hidden">
        <p className="text-right text-secondary text-sm font-semibold">
          -- STANLEY HARRISON
        </p>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 400.22 60.72"
          className="w-28 h-12 object-right float-right"
          fill="#e57676"
          stroke="none"
        >
          <defs />
          <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
              <path
                className="cls-1"
                d="M33.67 1.16l8.23 16.66A2 2 0 0043.46 19l18.39 2.68A2.07 2.07 0 0163 25.17l-13.31 13A2 2 0 0049.1 40l3.14 18.32a2.07 2.07 0 01-3 2.18l-16.46-8.67a2.06 2.06 0 00-1.93 0L14.4 60.47a2.07 2.07 0 01-3-2.18L14.53 40a2 2 0 00-.6-1.83l-13.3-13a2.07 2.07 0 011.15-3.54L20.17 19a2.06 2.06 0 001.56-1.13L30 1.16a2.07 2.07 0 013.67 0zM117.82 1.16l8.23 16.66a2 2 0 001.56 1.18L146 21.63a2.07 2.07 0 011.15 3.54l-13.31 13a2.07 2.07 0 00-.6 1.83l3.14 18.32a2.07 2.07 0 01-3 2.18l-16.44-8.64a2.06 2.06 0 00-1.93 0l-16.46 8.61a2.07 2.07 0 01-3-2.18L98.68 40a2.07 2.07 0 00-.6-1.83l-13.3-13a2.07 2.07 0 011.15-3.54L104.31 19a2.07 2.07 0 001.57-1.13l8.22-16.71a2.07 2.07 0 013.72 0zM202 1.16l8.22 16.66a2.07 2.07 0 001.54 1.18l18.38 2.68a2.07 2.07 0 011.15 3.54l-13.3 13a2.07 2.07 0 00-.6 1.83l3.14 18.32a2.07 2.07 0 01-3 2.18l-16.45-8.64a2.06 2.06 0 00-1.93 0l-16.45 8.56a2.07 2.07 0 01-3-2.18L182.83 40a2.07 2.07 0 00-.6-1.83l-13.31-13a2.07 2.07 0 011.15-3.54L188.46 19a2 2 0 001.54-1.18l8.23-16.66a2.07 2.07 0 013.77 0zM286.12 1.16l8.22 16.66A2.06 2.06 0 00295.9 19l18.39 2.68a2.07 2.07 0 011.15 3.54l-13.3 13a2 2 0 00-.6 1.83l3.14 18.32a2.07 2.07 0 01-3 2.18l-16.45-8.64a2.06 2.06 0 00-1.93 0l-16.45 8.64a2.07 2.07 0 01-3-2.18L267 40a2 2 0 00-.59-1.83l-13.31-13a2.07 2.07 0 011.15-3.54L272.61 19a2 2 0 001.56-1.13l8.23-16.71a2.07 2.07 0 013.72 0zM370.27 1.16l8.22 16.66a2 2 0 001.56 1.18l18.39 2.68a2.07 2.07 0 011.15 3.54l-13.31 13a2 2 0 00-.59 1.83l3.14 18.32a2.07 2.07 0 01-3 2.18l-16.45-8.64a2.06 2.06 0 00-1.93 0L351 60.47a2.07 2.07 0 01-3-2.18L351.12 40a2.07 2.07 0 00-.59-1.83l-13.31-13a2.07 2.07 0 011.15-3.54L356.76 19a2.06 2.06 0 001.56-1.13l8.22-16.66a2.08 2.08 0 013.73-.05z"
              />
            </g>
          </g>
        </svg>
      </div>

      <div className="w-full text-center overflow-hidden">
        <button className="self-center object-center mx-auto py-3 px-7 mt-10 border-2 border-secondary bg-secondary font-bold text-white hover:border-white transition duration-500 ease-in-out">
          <a
            href="https://tailwindcss.com/docs"
            className="text-cyan-600 hover:text-cyan-700"
          >
            {' '}
            SEE MORE REVIEWS{' '}
          </a>
        </button>
      </div>
    </div>
  </div>
)

export default TestimonialBlock
