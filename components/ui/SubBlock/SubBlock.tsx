import s from './SubBlock.module.css'

const SubBlock = ({ className = '', ...props }) => (
  <div className="flex justify-center items-center content-center flex-wrap overflow-hidden mb-20  md:-mx-6 lg:-mx-6 xl:-mx-6">
    <div className="justify-center w-full overflow-hidden md:my-6 md:px-6 md:w-1/3 lg:my-6 lg:px-6 lg:w-1/3 xl:my-6 xl:px-6 xl:w-1/3">
      <img
        className="w-2/3 text-center mx-auto pt-20 px-20 pb-5 md:pt-0 md:px-0 md: pb:0 lg:pt-20 lg:px-20 lg:pb-5"
        src="/flavor.png"
      />
      <h3 className="text-center text-secondary font-bold text-xl">
        Choose your flavor.
      </h3>
      <p className="text-center font-bold px-32 md:px-0 lg:px-32">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
      </p>
    </div>

    <div className="mx-auto w-full justify-center overflow-hidden md:my-6 md:px-6 md:w-1/3 lg:my-6 lg:px-6 lg:w-1/3 xl:my-6 xl:px-6 xl:w-1/3">
      <img
        className="w-2/3 text-center mx-auto pt-20 px-20 pb-5 md:pt-0 md:px-0 md: pb:0 lg:pt-20 lg:px-20 lg:pb-5"
        src="/delivery.png"
      />
      <h3 className="text-center text-secondary font-bold text-xl">
        Get it delivered.{' '}
      </h3>
      <p className="text-center font-bold px-32 md:px-0 lg:px-32">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
      </p>
    </div>

    <div className="mx-auto w-full justify-center overflow-hidden md:my-6 md:px-6 md:w-1/3 lg:my-6 lg:px-6 lg:w-1/3 xl:my-6 xl:px-6 xl:w-1/3">
      <img
        className="w-2/3 text-center mx-auto pt-20 px-20 pb-5 md:pt-0 md:px-0 md: pb:0 lg:pt-20 lg:px-20 lg:pb-5"
        src="/energy.png"
      />
      <h3 className="text-center text-secondary font-bold text-xl">
        All-day energy and relief.
      </h3>
      <p className="text-center font-bold px-32 md:px-0 lg:px-32">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
      </p>
    </div>

    <button className="py-3 px-7 mt-20 w-max border-2 border-secondary bg-secondary font-bold text-white hover:border-white transition duration-500 ease-in-out">
      <a
        href="https://tailwindcss.com/docs"
        className="text-cyan-600 hover:text-cyan-700"
      >
        {' '}
        SUBCRIBE NOW{' '}
      </a>
    </button>
  </div>
)

export default SubBlock
